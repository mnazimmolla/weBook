-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2018 at 11:29 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webook`
--

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `like` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_06_09_114020_create_users_table', 1),
(2, '2017_06_10_184841_create_posts_table', 2),
(3, '2017_06_17_190223_create_likes_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `body`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'orem ipsum dolor sit amet', 2, '2017-06-11 04:43:06', '2017-06-11 04:43:06'),
(6, 'this is admin post', 1, '2017-06-12 20:17:31', '2017-06-12 20:17:31'),
(7, 'test', 2, '2017-06-14 03:56:40', '2017-06-14 03:56:40'),
(9, 'this is test', 6, '2017-07-06 11:12:23', '2017-07-06 11:12:23'),
(10, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\'', 7, '2017-07-07 11:09:27', '2017-07-07 11:09:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `first_name`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', 'Admin', '$2y$10$CZnIlbmVvOScQ3vUpVHkUOjV2g8Wy/CrpPPNK5buDXZmxbVRghboK', 'IXgYtMOnyYE7ooivwFzu4imdb3SjAIMHwAIJGtHPq3K15ZtkrpwgxqsKbh9v', '2017-06-09 09:18:51', '2017-06-09 09:18:51'),
(2, 'test@gmail.com', 'Test', '$2y$10$0ugXMzmaIWlH5F.UAjATreqRGdR5uISNFeiiyRJQz3SkKEYzdJjx.', 'XewEQThweCIT34aNTkY3VLZzOClfWqU1JRNGmkY2HHYTQGT7jNNAtiutpSaq', '2017-06-09 10:09:28', '2017-06-09 10:09:28'),
(3, 'test1@gmail.com', 'Test1', '$2y$10$nGBDxLCfm6xZ7kJTjsji1uaGPgUMRNvC45c9fjxmTDLZ6XcoJjcC6', NULL, '2017-06-09 10:13:42', '2017-06-09 10:13:42'),
(4, 'admin@gmail.com', 'Admin', '$2y$10$RRl91qlKOk4iiOPxY4an1.mRgb2ZGEytsrCPPWXoEzKqQbdrpd40C', NULL, '2017-06-09 10:22:27', '2017-06-09 10:22:27'),
(5, 'admin@gmail.com', 'Admin', '$2y$10$bYPMzBIvRttHYm1wpUsqAOsAbYZkKHXfhlvug7ZxVT1WwStVAwKz.', NULL, '2017-06-09 10:27:18', '2017-06-09 10:27:18'),
(6, 'nazimleo@gmail.com', 'Nazim', '$2y$10$TOt2cpiZJKcOsn/YxWeFe.zfTZS4z0QV36HRxDB.EGQIv/y/c.Eda', 'OZrzKUBjo5azHSsKbMTTooo9cKx9wuAPE7qZmGsyq1AzrueJ68b0ADlGS6LB', '2017-07-06 10:27:13', '2017-07-06 10:27:13'),
(7, 'testrr@gmail.com', 'Test', '$2y$10$q6bxy5NovMYfpXLmWKb/reTVCxSDlI8AwORMZC.HFbztcvtvckX0y', '56wCD9HfUlYp6bo7Isikqh2DwZ43uU3kk9jTUomuF5c1QfP3NpUOKPMBooFo', '2017-07-07 11:08:42', '2017-07-07 11:08:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
