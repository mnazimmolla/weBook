var postId = 0;
var postBodyElement = null;
$('.post').find('.interection').find('.edit').on('click', function (event) {
    event.preventDefault();

    postBodyElement = event.target.parentNode.parentNode.childNodes[1];
    var postBody = postBodyElement.textContent;
    postId = event.target.parentNode.parentNode.dataset['postId'];
    $('#post-body').val(postBody);
    $('#edit-modal').modal();
});
$('#modal-save').on('click', function () {
    $.ajax({
        method: 'POST',
        url: urlEdit,
        data: {body: $('#post-body').val(), postId:postId, _token:token},
    })
    .done(function (msg) {
        $(postBodyElement).text(msg['new_body']);
        $('#editModal').modal(hide);
    })
});
$('.like').on('click', function(event){
    event.preventDefault();
    postId = event.target.parentNode.parentNode.dataset['postId'];
    var islike = event.target.previousElementSibling == null;
    console.log(islike);
    $.ajax({
        method : 'POST',
        url : urlLike,
        data : {islike: islike, postId: postId, _token: token}
    })
        .done(function () {
            event.target.innerText = islike ? event.target.innerText == 'Like' ? 'You Liked This Post' : 'Like' : event.target.innerText == 'Dislike' ? 'You don\'t Liked This Post' : 'Dislike';
            if(islike){
                event.target.nextElementSibling.innerText = 'Dislike';
            }else {
                event.target.previousElementSibling.innerText = 'Like';
            }
        });
});