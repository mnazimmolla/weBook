<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');
Route::post('/singup', 'UserController@postSingUp');
Route::get('/dashboard', [
    'uses' => 'PostController@getDashboard',
    'as' => 'dashboard',
]);

Route::post('/singin', [
    'uses' => 'UserController@postSingIn',
    'as' => 'singin',
]);
Route::get('/login', function () {
    echo "Hello";
});

Route::post('/create.post', 'PostController@postCreatePost');
Route::post('/login', [
    'as' => 'login',
]);
Route::get('/account.image/{filename}', [
    'uses' => 'UserController@getUserImage',
    'as' => 'account.image',
]);
Route::post('/like', 'PostController@likePost');
Route::get('/logout', 'UserController@getLogout');
Route::get('/post.delete/{id}', 'PostController@getDeletePost');
Route::get('/account', 'UserController@getAccount');
Route::post('/updateaccount', 'UserController@postSaveAccount');
Route::post('/edit', 'PostController@postEditPost')->name('edit');