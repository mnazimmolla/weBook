@extends('layouts.master')

@section('content')
    @include('includes.message-block')
    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>What do you want to say?</h3></header>
            <form action="create.post" method="post">
                <div class="form-group">
                    <textarea name="body" id="new-post" class="form-control" rows="6"
                              placeholder="Your Post"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Create Post</button>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </section>


    <section class="row posts">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>What other people say</h3></header>

            @foreach($posts as $post)
                <div class="post" data-postid="{{$post->id}}">
                    <p>{{ $post->body }}</p>
                    <div class="info">
                        Post by {{ $post->user->first_name }} on {{ $post->created_at }}
                    </div>
                    <div class="interection">
                        <a href="#" class="like">{{ Auth::user()->likes()->where('post_id', $post->id)->first() ? Auth::user()->likes()->where('post_id', $post->id)->first()->like == 1 ? 'You Liked This Post' : 'Like' : 'Like' }}</a>|
                        <a href="#" class="like">{{ Auth::user()->likes()->where('post_id', $post->id)->first() ? Auth::user()->likes()->where('post_id', $post->id)->first()->like == 0 ? 'You don\'t Liked This Post' : 'Dislike' : 'Dislike' }}</a>|
                        @if(Auth::user() == $post->user)
                            <a data-toggle="modal" data-target="#editModal" href="#" class="edit">Edit</a>|

                            <a href="{{url('post.delete/' .$post->id)}}">Delete</a>
                        @endif
                    </div>
                    </article>
                </div>
            @endforeach

        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Your Post</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="post-body">Edit The Post</label>
                            <textarea class="form-control" name="post-body" id="post-body" cols="5" rows="5"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="modal-save">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
